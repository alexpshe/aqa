package lesson4.theory;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Phone phone1 = new Phone();
        Phone phone2 = new Phone("3535-27", Model.IPHONE, 100);
        Phone phone3 = new Phone("156-273", Model.SAMSUNG);

        Contact fred = new Contact("Fred", "756-657");
        Contact tom = new Contact("Tom", "756-464");

        phone1.receiveCall(fred);
        phone1.receiveCall(tom, "21-12-2021");

        phone1.sendMessage(Arrays.asList(fred, tom), "Привет, как дела?");
        phone2.printFields();
    }
}
