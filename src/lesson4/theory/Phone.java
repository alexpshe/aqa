package lesson4.theory;

import java.util.List;

public class Phone {
    private String number;
    private Model model;
    private double weight;

    public Phone(String number, Model model, double weight) {
        this(number, model);
        this.weight = weight;
    }

    public Phone(String number, Model model) {
        this.number = number;
        this.model = model;
    }

    public Phone() {
    }

    public void printFields() {
        System.out.println("Номер:" + number);
        System.out.println("Модель:" + model);
        System.out.println("Вес:" + weight);
    }

    public void receiveCall(Contact contact) {
        System.out.println("Звонит " + contact.getName() + " с номером " + contact.getNumber());
    }

    public void receiveCall(Contact contact, String date) {
        System.out.println("Звонит " + contact.getName() +
                " с номером " + contact.getNumber() +
                " и дата " + date);
    }

    public void sendMessage(List<Contact> contacts, String message) {
        for (Contact contact : contacts) {
            System.out.println("Отправить сообщение " + message +
                    " контакту: Имя " + contact.getName() + " номер " + contact.getNumber());
        }
    }
}
