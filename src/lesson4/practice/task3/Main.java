package lesson4.practice.task3;

/**
 * Описать класс, реализующий десятичный счетчик,
 * который может увеличивать или уменьшать свое значение на единицу в заданном диапазоне.
 * Предусмотреть инициализацию счетчика значениями
 * по умолчанию и произвольными значениями.
 * Счетчик имеет два метода: увеличения и уменьшения,
 * — и свойство, позволяющее получить его текущее состояние.
 *
 * Написать программу, демонстрирующую все возможности класса.
 */
public class Main {
    public static void main(String[] args) {
        Counter counter1 = new Counter();
        Counter counter2 = new Counter(2);

        counter1.increaseValue();
        System.out.println(counter1.getCurrentValue());

        counter2.decreaseValue();
        System.out.println(counter2.getCurrentValue());
    }
}
