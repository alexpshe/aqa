package lesson4.practice.task4;

/**
 *  Реализовать класс кошка с перегруженными конструкторами.
 */
public class Main {
    public static void main(String[] args) {
        Cat noNameCat = new Cat();
        Cat namedHomelessCat = new Cat("Tom");
        Cat namedHomeCat = new Cat("Fred", CatType.HOME);

        noNameCat.printCatInfo();
        namedHomeCat.printCatInfo();
        namedHomelessCat.printCatInfo();
    }
}
