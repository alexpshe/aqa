package lesson4.practice.task4;

import java.util.Random;

public class Cat {
    private String name;
    private CatType type;

    public Cat(String name, CatType type) {
        this.name = name;
        this.type = type;
    }

    public Cat() {
        this("cat_" + new Random().nextInt(10), CatType.HOMELESS);
    }

    public Cat(String name) {
        this(name, CatType.HOMELESS);
    }

    public void printCatInfo() {
        System.out.println("Name " + this.name + ", type " + this.type);
    }
}
