package lesson4.practice.task2;

/**
 * Создать класс с двумя переменными.
 * Добавить функцию вывода на экран и функцию изменения этих переменных.
 * Добавить функцию, которая находит сумму значений этих переменных,
 * и функцию которая находит наибольшее значение из этих двух переменных.
 */
public class Main {
    public static void main(String[] args) {
        Variable variable = new Variable();
        variable.setA(2);
        variable.setB(3);

        variable.printA();
        variable.printB();

        System.out.println(variable.sum());
        System.out.println(variable.max());
    }
}
