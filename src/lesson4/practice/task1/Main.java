package lesson4.practice.task1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Создайте структуру с именем student,содержащую поля:
 * фамилия и инициалы,
 * номер группы,
 * успеваемость (массив из пяти элементов).
 *
 * Создать массив из десяти элементов такого типа,
 * вывести средний балл.
 */
public class Main {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>(3);

        students.add(new Student("Fred", "AF", 2, Arrays.asList(1, 2, 3)));
        students.add(new Student("Tom", "AT", 2, Arrays.asList(3, 2, 3)));
        students.add(new Student("Kate", "AK", 2, Arrays.asList(5, 2, 3)));

        for (Student student : students) {
            System.out.println("Средняя оценка: " + student.averageMark() +
                    ", номер группы: " + student.getGroupNumber());
        }
    }
}
