package lesson4.practice.task5;

/**
 * Создать класс, содержащий динамический массив и количество элементов в нем.
 * Добавить конструктор, который выделяет память под заданное количество элементов.
 * Добавить методы, позволяющие заполнять массив случайными числами.
 */
public class Main {
    public static void main(String[] args) {
        DynamicArray emptyDynamicArray = new DynamicArray();

        DynamicArray prefilledDynamicArray = new DynamicArray(3);

        emptyDynamicArray.generateRandomData(4);

        prefilledDynamicArray.generateRandomData();
    }
}
