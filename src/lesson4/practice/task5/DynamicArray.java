package lesson4.practice.task5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class DynamicArray {
    private List<Integer> list;

    public DynamicArray() {
        this.list = new ArrayList<>();
    }

    public DynamicArray(int size) {
        this.list = Arrays.asList(new Integer[size]);
    }

    public void generateRandomData(int size) {
        Random random = new Random();
        for(int i = 0 ; i < size; i++) {
            list.add(random.nextInt(100));
        }
    }

    public void generateRandomData() {
        Random random = new Random();
        for(int i = 0 ; i < list.size(); i++) {
            list.set(i, random.nextInt(100));
        }
    }
}
