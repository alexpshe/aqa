package lesson3;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;

public class LessonTasks {
    public static void main(String[] args) {
        // Set creation task



        int k = 1;

        List<Integer> numbers = Arrays.asList(0, 3, 3, 4, 4);
        Set<Integer> result = setCreation(numbers);

        // Two sets tasks
        Set<Integer> set1 = Set.of(2, 3, 4);
        Set<Integer> set2 = Set.of(2, 3);
        twoSets(set1, set2);

        twoNumbers();
        systemOfRegistration();
        medianOfTwoArrays();
    }

    public static void twoNumbers() {
//        Задача №1: 2 числа
//
//        Учитывая массив целых чисел nums и целое число target,
//        верните индексы двух чисел так, чтобы они в сумме равнялись target .
//
//
//        Пример 1:
//        Ввод: nums = [2,7,11,15], target = 9
//        Вывод: [0,1]
//        Вывод: Поскольку nums[0] + nums[1] == 9, мы возвращаем [0, 1]

        List<Integer> nums = Arrays.asList(2, 3);
        Integer target = 1;
        boolean flag = false;

        for (int i = 0; i < nums.size(); i++) {
            for (int j = i + 1; j < nums.size(); j++) {
                if (nums.get(i) + nums.get(j) == target) {
                    System.out.println("Индекс " + i + ", индекс " + j);
                    flag = true;
                    break;
                }
            }
            if (flag) {
                break;
            }
        }

        if (!flag) {
            System.out.println("Таких индексов нет");
        }
    }

    public static Set<Integer> setCreation(List<Integer> numbers) {
//        Задача №2: Составление множества
//
//        Дано натуральный набор чисел.
//        Составить множество из набора чисел,
//        если в списке несколько одинаковых чисел,
//        то добавлять в множество число формата "%число%количество повторений"

        Set<Integer> resultNumbers = new HashSet<>();
        Set<Integer> valuesWithRepeats = new HashSet<>();


        for (int i = 0; i < numbers.size(); i++) {
            if (valuesWithRepeats.contains(numbers.get(i))) {
                continue;
            }

            int count = 0;

            // повторений
            for (int j = 0; j < numbers.size(); j++) {
                if (numbers.get(i) == numbers.get(j)) {
                    count++;
                }
            }

            if (count == 1) {
                resultNumbers.add(numbers.get(i));
            } else {
                String result = numbers.get(i).toString() + count;
                resultNumbers.add(Integer.valueOf(result));
                valuesWithRepeats.add(numbers.get(i));
            }
        }

        return resultNumbers;
    }

    public static Set<Integer> twoSets(Set<Integer> set1, Set<Integer> set2) {
//        Задача №3:  Пересечение множества
//        Сформировать пересечение двух множеств.

        Set<Integer> result = new HashSet<>();

        set1.forEach(element1 ->
                set2.forEach(element2 -> {
                    if (Objects.equals(element1, element2)) {
                        result.add(element1);
                    }
                })
        );

        return result;
    }

    public static void systemOfRegistration() {
//        Задача №4: Система регистрации
//        В скором времени в Берляндии откроется новая почтовая служба "Берляндеск".
//        Администрация сайта хочет запустить свой проект как можно быстрее,
//        поэтому они попросили Вас о помощи. Вам предлагается реализовать
//        прототип системы регистрации сайта.
//                Система должна работать по следующему принципу.
//                Каждый раз, когда новый пользователь хочет зарегистрироваться,
//                он посылает системе запрос name со своим именем.
//                Если данное имя не содержится в базе данных системы,
//                то оно заносится туда и пользователю возвращается ответ OK,
//                подтверждающий успешную регистрацию. Если же на сайте уже присутствует
//                пользователь с именем name, то система формирует новое имя и выдает его пользователю

        String newName = "Kate";

        Set<String> names = new HashSet<>();
        names.add("Kate");

        if (!names.contains(newName)) {
            names.add(newName);
            System.out.println("OK");
        } else {
            Random random = new Random();
            String generatedName = newName;
            while (names.contains(generatedName)) {
                generatedName = newName + random.nextInt(10);
            }
            names.add(generatedName);
            System.out.println("Извините, имя " + newName + " занято, ваше имя " + generatedName);
        }
    }

    public static void medianOfTwoArrays() {
//        Задча №2: Медиана массивов
//        Для двух отсортированных массивов nums1 и nums2
//        размера m и, n соответственно,
//        вернуть медианное значение двух отсортированных массивов.
//        Пример 1:
//        Ввод: nums1 = [1,3], nums2 = [2] = [1,2,3]
//        Вывод: 2.00000
//        Объяснение: объединенный массив = [1,2,3], а медиана равна 2.

        List<Integer> list1 = Arrays.asList(1, 3);
        List<Integer> list2 = Arrays.asList(2);
        List<Integer> result = new ArrayList<>();

        int pointer1 = 0;
        int pointer2 = 0;

        while (pointer1 < list1.size() && pointer2 < list2.size()) {
            if (list1.get(pointer1) < list2.get(pointer2)) {
                result.add(list1.get(pointer1));
                pointer1++;
            } else {
                result.add(list2.get(pointer2));
                pointer2++;
            }
        }

        if (pointer1 < list1.size()) {
            while (pointer1 < list1.size()) {
                result.add(list1.get(pointer1));
                pointer1++;
            }
        }

        if (pointer2 < list2.size()) {
            while (pointer2 < list2.size()) {
                result.add(list2.get(pointer2));
                pointer2++;
            }
        }

        System.out.println(result);
    }

}
