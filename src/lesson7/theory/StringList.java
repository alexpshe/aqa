package lesson7.theory;

import java.util.Iterator;

public interface StringList<Type> {
   void add(Type x);
   Iterator<Type> iterator();
}

