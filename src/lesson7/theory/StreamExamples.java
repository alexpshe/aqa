package lesson7.theory;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamExamples {
    public static void main(String[] args) {
        int n = 10;
        IntStream.range(0, n)
                .filter(x -> x % 2 == 1)
                .map(x -> x * x)
                .forEach(System.out::println);

        int count = (int) Stream.of(2, 3, 0, 1, 3)
                .flatMapToInt(x -> IntStream.range(0, x))
                .count();
        // 0, 1, 0, 1, 2, 0, 0, 1, 2

        Stream.of(1, 2, 3, 4, 5, 6)
                .flatMap(x -> {
                    switch (x % 3) {
                        case 0:
                            return Stream.of(x, x * x, x * x * x);
                        case 1:
                            return Stream.of(x);
                        case 2:
                        default:
                            return Stream.empty();
                    }
                })
                .forEach(System.out::println);
        // 1, 3, 9, 18, 4, 6, 36, 72
    }
}
