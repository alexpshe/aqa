package lesson7.theory;

import java.util.ArrayList;

public class Main {

    public static <T> Entry<T, T> twice(T value) {
        return new Entry<>(value, value);
    }

    public static void main(String[] args) {
        ArrayList list = new ArrayList<String>();
        ArrayList list2 = new ArrayList<Integer>();


        Entry entry = new Entry("key", "value");
        Entry<Integer, Integer> entry1 = new Entry<>(1, 2);
        Entry<String, Long> population = new Entry<>("Спб", 100000000L);


        Entry<String, String> twiceValue = twice("key");
        Entry<Integer, Integer> twiceShort = twice(1);

        System.out.println(twiceShort);
    }
}
