package lesson7.practice.exceptions;

public class IncorrectCode {
    public static void main(String[] args) throws DivisionByZero {
        operateNonZeroValue();
    }

    public  static void operateNonZeroValue() throws DivisionByZero {
        int value = 0;
        processNonZeroValue(value);
    }

    public static void processNonZeroValue(int nonZero) throws DivisionByZero {
        if (nonZero == 0) {
            throw new DivisionByZero("nonZero value should not be zero");
        }
    }
}
