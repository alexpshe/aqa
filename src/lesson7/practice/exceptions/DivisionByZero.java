package lesson7.practice.exceptions;

public class DivisionByZero extends Exception {
    public DivisionByZero(String message) {
        super(message);
    }
}
