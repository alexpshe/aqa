package lesson7.practice;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Long> list = List.of(1L, 2L);
        List<Long> filteredList = filterGroupByParity(list);
        System.out.println(filteredList);

        List<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        List<Integer> list2 = new ArrayList<>();
        list2.add(3);
        list2.add(4);

        List<Integer> newList = joinGroup(list1, list2);
        System.out.println(newList);

        List<String> listStr1 = new ArrayList<>();
        listStr1.add("1");
        listStr1.add("2");
        List<String> listStr2 = new ArrayList<>();
        listStr2.add("3");
        listStr2.add("4");

        List<String> newListStr = joinGroup(listStr1, listStr2);
        System.out.println(newListStr);
    }

    /**
     * Generic-метод возвращает группу, в которой содержатся четные элементы из входной группы.
     *
     * @param group
     * @return
     */
    public static <Type> List<Type> filterGroupByParity(List<? extends Number> group) {
        List<Type> filterGroupByParity = new ArrayList<>();
        group.forEach(element -> {
            if (element.doubleValue() % 2 == 0) {
                filterGroupByParity.add((Type) element);
            }
        });
        return filterGroupByParity;
    }

    /**
     * Generic-метод принимает на вход 2 группы, объединяет группы и возвращает объединенную группу.
     *
     * @return
     */
    public static <Type> List<Type> joinGroup(List<Type> group1, List<Type> group2) {
        group1.addAll(group2);
        return group1;
    }
}
