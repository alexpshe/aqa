package lesson7.practice;

public class AnimalProcessor {
    public static void main(String[] args) {
        Cat cat = new Cat();
        Dog dog = new Dog();
        AnimalBuilder animalBuilderForCat = new AnimalBuilder<>();
        AnimalBuilder animalBuilderForDog = new AnimalBuilder<Dog>();

        animalBuilderForCat.goToHome(cat);
        animalBuilderForCat.goToHospital(cat);

        animalBuilderForDog.goToHome(dog);
        animalBuilderForCat.goToHospital(dog);
    }
}
