package lesson7.practice;

public class AnimalBuilder<Type> {
    public void goToHospital(Type animal) {
        System.out.println("Go to hospital " + animal.toString());
    }

    public void goToHome(Type animal) {
        System.out.println("Go to home " + animal.toString());
    }
}
