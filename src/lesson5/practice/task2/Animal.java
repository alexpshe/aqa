package lesson5.practice.task2;

public class Animal {
    private String food;
    private Location location;

    public Animal(String food, Location location) {
        this.food = food;
        this.location = location;
    }

    public void makeNoise() {}

    public void eat() {}

    public void sleep() {
        System.out.println("Животное спит");
    }

    public String getFood() {
        return food;
    }

    public Location getLocation() {
        return location;
    }
}
