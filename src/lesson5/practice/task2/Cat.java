package lesson5.practice.task2;

public class Cat extends Animal {
    public Cat(String food, Location location) {
        super(food, location);
    }

    @Override
    public void makeNoise() {
        System.out.println("Кошка мурчит");
    }

    @Override
    public void eat() {
        System.out.println("Кошка ест " + super.getFood());
    }
}
