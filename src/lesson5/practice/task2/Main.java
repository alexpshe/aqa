package lesson5.practice.task2;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Animal> animals = new ArrayList<>();
        animals.add(new Cat("кошачий корм", Location.HOME));
        animals.add(new Dog("кость", Location.HOME));
        animals.add(new Horse("сено", Location.AFRICA));

        Veterinarian veterinarian = new Veterinarian();

        for(Animal animal : animals) {
            veterinarian.treatAnimal(animal);
        }
    }
}
