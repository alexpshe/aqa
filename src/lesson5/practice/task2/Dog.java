package lesson5.practice.task2;

public class Dog extends Animal{
    public Dog(String food, Location location) {
        super(food, location);
    }

    @Override
    public void makeNoise() {
        System.out.println("Пёс лает");
    }

    @Override
    public void eat() {
        System.out.println("Пёс ест " + super.getFood());
    }
}
