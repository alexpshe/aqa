package lesson5.practice.task2;

public class Veterinarian {
    public void treatAnimal(Animal animal) {
        System.out.println("Животное пришло на приём: ест " + animal.getFood() +
                ", живёт в " + animal.getLocation());
    }
}
