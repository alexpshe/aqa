package lesson5.practice.task2;

public class Horse extends Animal {
    public Horse(String food, Location location) {
        super(food, location);
    }

    @Override
    public void makeNoise() {
        System.out.println("Лошадь брезжит");
    }

    @Override
    public void eat() {
        System.out.println("Лошадь ест " + super.getFood());
    }
}
