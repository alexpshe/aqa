package lesson5.practice.task1;

public class Main {
    public static void main(String[] args) {
        Engine engine = new Engine(100, "Kia");
        Driver driver = new Driver(30, "Tom Hardi", 10);
        Car car = new Car("M1", engine, driver, "Kia");
        Lorry lorry = new Lorry("W1", engine, driver, "Kia", 1000);

        MoveDriver moveDriver = new MoveDriver();
        moveDriver.move(lorry);
    }
}
