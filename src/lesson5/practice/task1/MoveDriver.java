package lesson5.practice.task1;

public class MoveDriver {
    public void move(Car car) {
        car.start();
        car.turnRight();
        car.turnLeft();
        car.stop();
    }
}
