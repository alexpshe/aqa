package lesson5.pheory;

public class Main {
    public static void main(String[] args) {
        Employee tom = new Manager("Tom");
        Employee fred = new Manager("Fred");
        Employee hew = new Manager("Hew");

        Cat cat1 = new Cat("Tom", CatType.HOMELESS, 4);
        Cat cat2 = new Cat("Tom", CatType.HOMELESS, 4);

        QaEngineer qa = new QaEngineer("Greg");
        Manager manager = new Manager("John");

        qa.setInVacation(false);
        qa.setTired(false);

        manager.work();
        qa.work();
        qa.relax();
        manager.relax();

        System.out.println();

        // наследование от класс - из родителя объекты
        // наследовать от класса, но не создать объект из родителя - абстрактный
        // если общее поведение - интерфейс
    }
}
