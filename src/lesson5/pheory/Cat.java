package lesson5.pheory;

import java.util.Objects;

public class Cat {
    private String name;
    private CatType type;
    private int countOfLags;

    public Cat(String name, CatType type, int countOfLags) {
        this.name = name;
        this.type = type;
        this.countOfLags = countOfLags;
    }

    @Override
    public String toString() {
        return "Cat with name: " +
                this.name + ", type: " + this.type +
                " countOfLags: " + this.countOfLags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cat cat = (Cat) o;
        return countOfLags == cat.countOfLags && Objects.equals(name, cat.name) && type == cat.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type, countOfLags);
    }
}
