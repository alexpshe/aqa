package lesson5.pheory;

public interface Workable {
    public void work();
    public boolean isTired();
    public void relax();
    public boolean getVacation();
}
