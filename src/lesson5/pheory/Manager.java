package lesson5.pheory;

public class Manager extends Employee implements Workable  {
    private static final int DEFAULT_MANAGER_SALARY = 1000;
    private int bonus;
    private Employee mentor;

    public Manager(String name) {
        super(name);
    }

    @Override
    public int getSalary() {
        return DEFAULT_MANAGER_SALARY;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    @Override
    public boolean studentFor(Employee e) {
        return e.getName().equals(mentor.getName());
    }

    @Override
    public void setMentor(Employee employee) {
        this.mentor = employee;
    }

    @Override
    public void work() {
        System.out.println("Менеджер оветаймит");
    }

    @Override
    public boolean isTired() {
        return true;
    }

    @Override
    public void relax() {
        System.out.println("Менеджер не отдыхает");
    }

    @Override
    public boolean getVacation() {
        return false;
    }
}
