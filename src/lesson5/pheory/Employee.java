package lesson5.pheory;

public abstract class Employee {
    private String name;

    public Employee(String name){
        this.name = name;
    }

    public final String getName() {
        return name;
    }

    public abstract int getSalary();

    public abstract boolean studentFor(Employee e);

    public abstract void setMentor(Employee employee);
}
