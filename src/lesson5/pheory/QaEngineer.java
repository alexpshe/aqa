package lesson5.pheory;

public class QaEngineer extends Employee implements Workable {
    private boolean isTired;
    private boolean isInVacation;

    public QaEngineer(String name) {
        super(name);
    }

    public void setTired(boolean tired) {
        isTired = tired;
    }

    public void setInVacation(boolean inVacation) {
        isInVacation = inVacation;
    }

    @Override
    public int getSalary() {
        return 0;
    }

    @Override
    public boolean studentFor(Employee e) {
        return false;
    }

    @Override
    public void setMentor(Employee employee) {

    }

    @Override
    public void work() {
        System.out.println("QA работает 8 часов в день");
    }

    @Override
    public boolean isTired() {
        return this.isTired;
    }

    @Override
    public void relax() {
        System.out.println("QA отдыхает каждый год");
    }

    @Override
    public boolean getVacation() {
        return this.isInVacation;
    }
}
