package lesson8.theory.abstractfactory;

public class Main {
    public static void main(String[] args) {
        Application application = new Application(new WindowsFactory());
        application.paint();
    }
}
