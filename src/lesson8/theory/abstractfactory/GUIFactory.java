package lesson8.theory.abstractfactory;

import lesson8.theory.abstractfactory.Button;
import lesson8.theory.abstractfactory.Checkbox;

/**
 * Абстрактная фабрика знает обо всех (абстрактных) типах продуктов.
 */
public interface GUIFactory {
    Button createButton();
    Checkbox createCheckbox();
}
