package lesson8.theory.fabricmethod;

public class Main {
    public static void main(String[] args) {
        HtmlDialog htmlDialog = new HtmlDialog();
        htmlDialog.renderWindow();

        WindowsDialog windowsDialog = new WindowsDialog();
        windowsDialog.renderWindow();

        System.out.println();
    }
}
