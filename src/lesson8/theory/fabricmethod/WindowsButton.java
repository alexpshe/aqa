package lesson8.theory.fabricmethod;

/**
 * Реализация нативных кнопок операционной системы.
 */
public class WindowsButton implements Button {

    public void render() {
        System.out.println("Render windows button");
        onClick();
    }

    public void onClick() {
        System.out.println("Click on windows button!");
    }
}
