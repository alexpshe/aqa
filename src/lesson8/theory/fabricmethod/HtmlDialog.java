package lesson8.theory.fabricmethod;

/**
 * HTML-диалог.
 */
public class HtmlDialog extends Dialog {
    @Override
    public Button createButton() {
        return new HtmlButton();
    }
}
