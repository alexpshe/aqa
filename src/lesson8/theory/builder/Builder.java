package lesson8.theory.builder;

public interface Builder {
    public Builder setName(String name);
    public Builder setType(String type);
    public Builder setAge(int age);
    public Builder setWeight(int weight);
}
