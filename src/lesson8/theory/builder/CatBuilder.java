package lesson8.theory.builder;

public class CatBuilder implements Builder {
    private String name;
    private String type;
    private int age;
    private int weight;

    @Override
    public CatBuilder setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public CatBuilder setType(String type) {
        this.type = type;
        return this;
    }

    @Override
    public CatBuilder setAge(int age) {
        this.age = age;
        return this;
    }

    @Override
    public CatBuilder setWeight(int weight) {
        this.weight = weight;
        return this;
    }

    public Cat getResult() {
       return new Cat(this.name, this.type, this.age, this.weight);
    }
}
