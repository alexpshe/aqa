package lesson8.theory.builder;

public class Main {
    public static void main(String[] args) {
        Cat cat1 = new Cat("h", "f", 1, 100);
        Cat cat2 = new Cat("h", "", 0, 0);

        Cat cat3 = new CatBuilder()
                .setAge(1)
                .setType("")
                .setWeight(100)
                .setName("Kate")
                .getResult();
    }
}
