package lesson8.theory.prototype;

public class Cat {
    private String name;

    public Cat(String name) {
        this.name = name;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Cat(this.name);
    }
}
