package lesson6.hometask;

import java.util.ArrayList;
import java.util.List;

public class Main {
    /**
     *
     State:
     User
     Item

     Catalog
     Basket
     Actions:
     Item
     Buy from basket
     Add to basket

     Catalog
     List of items


     User ->
     See catalogs ->
     See items of the catalog ->
     Choose items ->
     Add to basket ->
     Buy from basket
     * @param args
     */
    public static void main(String[] args) {
        Account account = new Account(
                new User("oleg", "1234"),
                new Basket(),
                new Wallet("1223", "1234", 201));

        Catalog catalog1 = new Catalog("dresses");
        catalog1.addItem(new Item("dress 1", "Nike", 100));
        catalog1.addItem(new Item("dress 2", "Nike 2", 200));

        Catalog catalog2 = new Catalog("T-shirts");
        catalog2.addItem(new Item("T-shirt 1", "Nike", 100));
        catalog2.addItem(new Item("T-shirt 2", "Nike 2", 200));

        List<Catalog> catalogs = new ArrayList<>();
        catalogs.add(catalog1);
        catalogs.add(catalog2);

        Shop shop = new Shop(catalogs);

        Item newDress = shop.findCatalogByName("dresses").findItemByName("dress 2");
        account.getBasket().addItem(newDress);

        account.buyItems();
    }
}
