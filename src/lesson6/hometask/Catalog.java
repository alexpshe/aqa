package lesson6.hometask;

import java.util.ArrayList;
import java.util.List;

public class Catalog {
    private String name;
    private List<Item> items;

    public Catalog(String name) {
        this.name = name;
        this.items = new ArrayList<>();
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public void removeItem(Item item) {
        items.remove(item);
    }

    public String getName() {
        return name;
    }

    public List<Item> getItems() {
        return items;
    }

    public Item findItemByName(String name) {
        return items.stream()
                .filter(it -> it.getName().equals(name)).findFirst().get();
    }
}
