package lesson6.hometask;

import java.util.List;

public class Shop {
    private List<Catalog> catalogs;

    public Shop(List<Catalog> catalogs) {
        this.catalogs = catalogs;
    }

    public Catalog findCatalogByName(String name) {
        return catalogs.stream()
                .filter(it -> it.getName().equals(name)).findFirst().get();
    }
}
