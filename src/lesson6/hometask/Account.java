package lesson6.hometask;

public class Account {
    private User user;
    private Basket basket;
    private Wallet wallet;

    public Account(User user, Basket basket, Wallet wallet) {
        this.user = user;
        this.basket = basket;
        this.wallet = wallet;
    }

    public User getUser() {
        return user;
    }

    public Basket getBasket() {
        return basket;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public boolean buyItems() {
        int sum = basket.getItems().stream().mapToInt(Item::getPrice).sum();
        return wallet.buy(sum);
    }
}
