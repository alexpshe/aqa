package lesson6.hometask;

public class Wallet {
    private String cardNumber;
    private String pinCode;
    private int money;

    public Wallet(String cardNumber, String pinCode, int money) {
        this.cardNumber = cardNumber;
        this.pinCode = pinCode;
        this.money = money;
    }

    public boolean buy(int cost) {
        boolean successOperation;
        if (money >= cost) {
            money -= cost;
            successOperation = true;
        } else {
            successOperation = false;
        }
        return successOperation;
    }
}
