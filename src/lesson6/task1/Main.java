package lesson6.task1;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Customer customer1 = new Customer("Дарья", "Семенова", "Ивановна", "Москва", "2020304078777336", "038560568072");
        Customer customer2 = new Customer("Анастасия", "Тарасова", "Кирилловна", "Коломна", "5020304078777676", "038999568072");
        Customer customer3 = new Customer("Кирилл", "Попов", "Дмитриевич", "Белгород", "2022356778777673", "695432568072");
        Customer customer4 = new Customer("Александр", "Яшин", "Евгеньевич", "Тула", "5020304073965271", "563560568072");
        Customer customer5 = new Customer("Оливия", "Константинова", "Евгеньевич", "Борисовка", "5030304078777676", "038683456072");

        List<Customer> customers = Arrays.asList(customer1, customer2, customer3, customer4, customer5);

        // способ №1
        int currentMax = 0; // самая длинная фамилия на текущий момент - длина
        Customer currentMaxLengthLastnameCustomer = null; // самая длинная фамилия на текущий момент -  имя

        for(int i = 0; i < customers.size(); i++) {
            if (customers.get(i).getLastname().length() > currentMax) {
                currentMax = customers.get(i).getLastname().length();
                currentMaxLengthLastnameCustomer = customers.get(i);
            }
        }

        //System.out.println(currentMaxLengthLastnameCustomer.getFirstname());


        // способ №2
        for(Customer customer: customers) {
            if (customer.getLastname().length() > currentMax) {
                currentMax = customer.getLastname().length();
                currentMaxLengthLastnameCustomer = customer;
            }
        }

        // Пункт 2
        CustomerCollector customerCollector = new CustomerCollector(customers);
        customerCollector.printAddressForFilteredCustomers('5', 0);
    }
}
