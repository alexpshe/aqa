package lesson6.task2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Book> books = new ArrayList<>();
        books.add(new Book("Shanne Mcnaughton", "Symbol sustainable carrying gone zimbabwe ext lyrics, photograph collection believe prominent workout sexuality them, rarely markets remainder courts meetings kinase. ", new Date().getYear(), 14700));
        books.add(new Book("Lenore Lodge", "Practices records chargers downloading soviet politicians baltimore, sensor dialog sciences direction reduction hook second, tap brother indication rise walking remark dried, hanging. ", new Date().getYear(), 14700));
        books.add(new Book("Peggy", "Deon Anderton", new Date().getYear(), 14700));

        HomeLibrary homeLibrary = new HomeLibrary(books);

        Book newBook1 = new Book("New author", "New book", new Date().getYear(), 100);
        Book newBook2 = new Book("New author", "New book", new Date().getYear(), 100);

        homeLibrary.addBook(newBook1);
        homeLibrary.removeBook(newBook2);
        homeLibrary.getSortedByNameBooks();
        List<Book> sortedBooks = homeLibrary.getSortedByCountOfPagesBooks();
    }
}
