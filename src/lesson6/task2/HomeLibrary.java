package lesson6.task2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class HomeLibrary {
    private List<Book> books; // book1, book2, book3

    public HomeLibrary(List<Book> books) {
        this.books = books;
    }

    public void addBook(Book book) {
        books.add(book);
    }

    public void removeBook(Book book) {
        books.remove(book);
    }

    public List<Book> getSortedByNameBooks() {
        return sortBooks(Comparator.comparing(Book::getName));
    }

    public List<Book> getSortedByAuthorBooks() {
        return sortBooks(Comparator.comparing(Book::getAuthor));
    }

    public List<Book> getSortedByCountOfPagesBooks() {
        return sortBooks(Comparator.comparing(Book::getCountOfPages));
    }

    private List<Book> sortBooks(Comparator<Book> comparator) {
        List<Book> sortedByNameBooks = new ArrayList<>(books);
        sortedByNameBooks.sort(comparator);
        return sortedByNameBooks;
    }
}