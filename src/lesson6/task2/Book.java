package lesson6.task2;

import java.util.Objects;

public class Book {
    private String author;
    private String name;
    private int year;
    private int countOfPages;

    public Book(String author, String name, int year, int countOfPages) {
        this.author = author;
        this.name = name;
        this.year = year;
        this.countOfPages = countOfPages;
    }

    public String getAuthor() {
        return author;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }

    public int getCountOfPages() {
        return countOfPages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return year == book.year && countOfPages == book.countOfPages && Objects.equals(author, book.author) && Objects.equals(name, book.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(author, name, year, countOfPages);
    }
}
